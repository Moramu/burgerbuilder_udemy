import * as actionType from '../actions/actionTypes';
import { updateObject } from '../utility';


const initialState = {
    ingridients: null,
    error: false,
    totalPrice: 4,
    building: false,
}

const INGRIDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.4,
    meat: 1.3,
    bacon: 0.7,
}

const addIngridient = (state, action) => {
    const updatedingridient = { [action.ingridientName]: state.ingridients[action.ingridientName] + 1 }
    const updatedIngridients = updateObject(state.ingridients, updatedingridient)
    const updatedState = {
        ingridients: updatedIngridients,
        totalPrice: state.totalPrice + INGRIDIENT_PRICES[action.ingridientName],
        building: true,
    }
    return updateObject(state, updatedState);
}

const removeIngridient = (state, action) => {
    const updatedingridient = { [action.ingridientName]: state.ingridients[action.ingridientName] - 1 }
    const updatedIngridients = updateObject(state.ingridients, updatedingridient)
    const updatedState = {
        ingridients: updatedIngridients,
        totalPrice: state.totalPrice - INGRIDIENT_PRICES[action.ingridientName],
        building: true,
    }
    return updateObject(state, updatedState);
}

const setIngridients = (state, action) => {
    return updateObject(state, {
        ingridients: {
            salad: action.ingridients.salad,
            bacon: action.ingridients.bacon,
            cheese: action.ingridients.cheese,
            meat: action.ingridients.meat,

        },
        totalPrice: 4,
        error: false,
        building: false,
    })
}

const fetchIngridientsFailed = (state, action) => {
    return updateObject(state, { error: true })
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionType.ADD_INGRIDIENT: return addIngridient(state, action)
        case actionType.REMOVE_INGRIDIENT: return removeIngridient(state, action)
        case actionType.SET_INGRIDIENTS: return setIngridients(state, action)
        case actionType.FETCH_INGRIDIENTS_FAILED: return fetchIngridientsFailed(state, action)
        default: return state;
    }

}

export default reducer;
