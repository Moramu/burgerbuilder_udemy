import React, { Component } from "react";
import Hoc from '../../../hoc/Hoc';
import Button from '../../UI/Button/Button'

class OrderSummary extends Component {
    // This could be a functional component
    componentDidUpdate() {
        // console.log('[OrderSummary] DidUpdate')
    }

    render() {
        const ingridientsSummary = Object.keys(this.props.ingridients)
            .map(igKey => {
                return (
                    <li key={igKey}>
                        <span style={{ textTransform: 'capitalize' }}>{igKey}</span> : {this.props.ingridients[igKey]}
                    </li>
                )
            })

        return (
            <Hoc>
                <h3>Your Order</h3>
                <p>A delicious burger with the following ingridients:</p>
                <ul>
                    {ingridientsSummary}
                </ul>
                <p><strong>Total Price: {this.props.price.toFixed(2)}</strong></p>
                <p>Continue to Checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCancelled}>Cancel</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}>Continue</Button>
            </Hoc>
        );
    }
}




export default OrderSummary;