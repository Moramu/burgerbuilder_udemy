import React from 'react';
import clasess from './BuildControl.module.css'

const buildControl = (props) => (
	<div className={clasess.BuildControl}>
		<div className={clasess.Label}>{props.label}</div>
		<button className={clasess.Less} onClick={props.removed} disabled={props.disabled}>Less</button>
		<button className={clasess.More} onClick={props.added}>More</button>
	</div>
);

export default buildControl
