import React from 'react';
import clasess from './Spinner.module.css';

const spinner = () => (

    <div className={clasess.Loader}>Loading....</div>
)

export default spinner;