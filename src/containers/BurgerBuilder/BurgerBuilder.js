import React, { Component } from 'react';

import Hoc from '../../hoc/Hoc';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux';
import axios from '../../axios-order';
import * as action from '../../store/actions/index';




class BurgerBuilder extends Component {
	state = {
		purchasing: false,
	}

	componentDidMount() {
		console.log(this.props)
		this.props.onInitIngridients()

	}

	updatePurchaseState(ingridients) {
		const sum = Object.keys(ingridients)
			.map(igKey => {
				return ingridients[igKey]
			})
			.reduce((sum, el) => {
				return sum + el;
			}, 0)
		return sum > 0;
	}

	purchaseHandler = () => {
		if(this.props.isAuthenticated) {
			this.setState({ purchasing: true })
		} else {
			this.props.onSetAuthRedirectPath('/checkout')
			this.props.history.push('/auth')
		}
		
	}

	purchaseCancelHandler = () => {
		
		this.setState({ purchasing: false })
	}

	purchaseContinueHandler = () => {
		this.props.onInitPurchase();
		this.props.history.push('/checkout')
	}

	render() {
		const disabledInfo = {
			...this.props.ings
		}
		for (let key in disabledInfo) {
			disabledInfo[key] = disabledInfo[key] <= 0
		}
		let orderSummary = null

		let burger = this.props.error ? <p>Ingridients can't be loaded</p> : <Spinner />
		if (this.props.ings) {
			burger = (
				<Hoc>
					<Burger ingridients={this.props.ings} />
					<BuildControls
						ingridientAdded={this.props.onIngridientAdded}
						ingridientRemoved={this.props.onIngridientRemoved}
						disabled={disabledInfo}
						purchasable={this.updatePurchaseState(this.props.ings)}
						ordered={this.purchaseHandler}
						isAuth={this.props.isAuthenticated}
						price={this.props.price}
					/>
				</Hoc>
			);
			orderSummary = <OrderSummary
				ingridients={this.props.ings}
				purchaseCancelled={this.purchaseCancelHandler}
				purchaseContinued={this.purchaseContinueHandler}
				price={this.props.price}
			/>
		}
		return (
			<Hoc>
				<Modal
					show={this.state.purchasing}
					modalClosed={this.purchaseCancelHandler}
				>
					{orderSummary}
				</Modal>
				{burger}
			</Hoc>
		);
	}
}

const mapStateToProps = state => {
	return {
		ings: state.burgerBuilder.ingridients,
		price: state.burgerBuilder.totalPrice,
		error: state.burgerBuilder.error,
		isAuthenticated: state.auth.token != null
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onIngridientAdded: (ingName) => dispatch(action.addIngridient(ingName)),
		onIngridientRemoved: (ingName) => dispatch(action.removeIngridient(ingName)),
		onInitIngridients: () => dispatch(action.initIngridients()),
		onInitPurchase: () => dispatch(action.purchaseInit()),
		onSetAuthRedirectPath: (path) => dispatch(action.setAuthRedirectPath(path)),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));