import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://udemy-course-fbd20.firebaseio.com/'
})


export default instance;